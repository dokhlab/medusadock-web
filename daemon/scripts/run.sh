. /etc/profile.d/modules.sh

module load medusa

work=$1

cd $work

cp input/constraints constraints.txt
echo $MEDUSA_PARAMETER
medusa dock -p $MEDUSA_PARAMETER -i rec.pdb -m lig.mol2 -M binding_site.mol2 -c constraints.txt -o dock.pdb -S $RANDOM -R

