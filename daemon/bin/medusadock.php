<?php

require_once(__DIR__.'/mail_utils.php');
require_once(__DIR__.'/config.php');

$id = $argv[1];
$email = '';
$work = "$daemon_path/exec/$id";
//$medusa = "$daemon_path/bin/medusa";

//$remote_work = "$ftpfolder/$id";

//$conn = ftp_open();
//ftp_mkdir_recursive($conn, $remote_work) or die("Couldn't create $remote_work");
//ftp_close($conn);


//function get_file($local_file, $remote_file) {
//  global $ftphost;
//
//  $conn = ftp_open();
//  if (ftp_get($conn, $local_file, $remote_file, FTP_BINARY)) {
//    echo "Successfully written to $local_file\n";
//  } else {
//    echo "There was a problem in getting $local_file from $ftphost : $remote_file\n";
//  }
//  ftp_close($conn);
//}

//function put_file($remote_file, $local_file) {
//  global $ftphost;
//
//  $conn = ftp_open();
//  if (ftp_put($conn, $remote_file, $local_file, FTP_BINARY)) {
//    echo "Successfully written to $remote_file\n";
//  } else {
//    echo "There was a problem in putting $local_file to $ftphost : $remote_file\n";
//  }
//  ftp_chmod($conn, 0755, $remote_file);
//  ftp_close($conn);
//}

function set_inputs() {
//  global $email, $id, $daemon_path, $file_store, $work, $dbtable;
  global $email, $id, $daemon_path, $work, $dbtable;

  echo "Fetch Inputs of $id...\n";

  $conn = database_connect();

  $query = "select * from $dbtable where id='$id'";

  $stmt = $conn->prepare($query); 
  $stmt->execute(); 
  $row = $stmt->fetch(PDO::FETCH_ASSOC);

//  echo json_encode($row)."\n";

  if (!empty($row)) {
    // Create execution directories and input files
    $input_path = "$work/input";
    if (!file_exists($input_path)) {
      echo "Making directory: $input_path...\n";
      mkdir($input_path, 0777, true);
    }

    foreach ($row as $k => $v) {
      file_put_contents("$input_path/$k", $v);
    }

    $userid = $row['userid'];
    $query = "select id, email from users where id='$userid'";

    $stmt = $conn->prepare($query); 
    $stmt->execute(); 
    $row2 = $stmt->fetch(PDO::FETCH_ASSOC);

    if (empty($row2)) {
      $email = $row['email'];
    } else {
      $email = $row2['email'];
    }

    return true;
  }

  $conn = null;
}

function send_result_mail($success) {
  global $id, $email;
  $host = "https://dokhlab.med.psu.edu";

  if (!empty($email)) {
    if ($success) {
      $subject = "MedusaDock Task Finished";
      $message  = "Dear User,\n\n";
      $message .= "Thank you for using MedusaDock! Your Task $id has been finished. Please click this link ($host/medusadock/#/Task/$id) for details!\n\n";
      $message .= "Sincerely,\n";
      $message .= "MedusaDock Team";
    } else {
      $subject = "MedusaDock Task Failed";
      $message  = "Dear User,\n\n";
      $message .= "Thank you for using MedusaDock! Your Task $id has failed. Please click this link ($host/medusadock/#/Task/$id) for details!\n\n";
      $message .= "Sincerely,\n";
      $message .= "MedusaDock Team";
    }
    send_mail($email , $subject , $message);
  }
}

function set_binding_site_file() {
  global $id, $daemon_path, $babel_path, $work;

  $binding_site = file_get_contents("$work/input/binding_site");
  $binding_site_pdb = "$work/binding_site.pdb";
  $binding_site_mol2 = "$work/binding_site.mol2";

  $pos = array_map(function ($s) { return floatval($s); }, preg_split("/\s+/", trim($binding_site)));
  echo "binding_site_pdb: $binding_site_pdb\n";
  echo "binding_site_mol2: $binding_site_mol2\n";
  file_put_contents($binding_site_pdb, sprintf('%-6s%5d  %-4s%3s%2s%4d    %8.3f%8.3f%8.3f', 'ATOM', 1, 'P', 'A', 'A', 1, $pos[0], $pos[1], $pos[2]));

  $cmd = "$babel_path -i pdb $binding_site_pdb -o mol2 $binding_site_mol2";
  echo "$cmd\n";
  shell_exec($cmd);

  return $binding_site_mol2;
}

function medusadock() {
//  global $id, $email, $daemon_path, $file_store, $work, $medusa, $remote_work, $conn;
  global $id, $email, $daemon_path, $work, $conn;

  database_update($id, ["status"=>"docking"], ["tprocess"=>"CURRENT_TIMESTAMP()"]);

  // Set receptor file
  $receptor_name = "$work/input/receptor";
  $receptor_file = "$work/rec.pdb";
  shell_exec("cp $receptor_name $receptor_file");

  // Set ligand file
  $ligand_name = "$work/input/ligand";
  $ligand_file = "$work/lig.mol2";
  shell_exec("cp $ligand_name $ligand_file");

  $binding_site_file = set_binding_site_file();
  $constraints_file = "$work/constraints";
  $output_file = "$work/dock.pdb";
  $log_file = "$work/log.txt";

  // Docking
  $cmd = "bash $daemon_path/scripts/run.sh $work >$log_file 2>&1";
  echo "Docking...\n";
  echo "Command: $cmd\n";
  shell_exec($cmd);

  // Put output file to ftp folder
  $output_name = "dock.pdb";
//  $output_name = "$remote_work/dock.pdb";
//  put_file($output_name, $output_file);

  // Put log file to ftp folder
  $log_name = "log.txt";
//  $log_name = "$remote_work/log.txt";
//  put_file($log_name, $log_file);

  echo "Docking finished\n";
  if (file_exists($output_file)) {
    echo "Updating database.\n";
    database_update($id, ["status"=>"finished", "output"=>$output_name, "log"=>$log_name], ["tfinish"=>"CURRENT_TIMESTAMP()"]);

    echo "Sending result email to $email.\n";
    send_result_mail(true);
  }
  else {
    echo "Updating database.\n";
    database_update($id, ["status"=>"failed", "log"=>$log_name]);

    echo "Sending result email to $email.\n";
    send_result_mail(false);
  }
}

set_inputs();
medusadock();

