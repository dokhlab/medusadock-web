<?php

require_once(__DIR__.'/mail_utils.php');

$config      = json_decode(file_get_contents(__DIR__.'/../../config.json'), true);

$dbhost      = $config['db']['host'];
$dbuser      = $config['db']['user'];
$dbpass      = $config['db']['password'];
$dbname      = $config['db']['database'];
$dbtable     = $config['db']['table'];

$file_store  = "/mnt/dokhlab";

//$ftphost      = $config['ftp']['host'];
//$ftpuser      = $config['ftp']['user'];
//$ftppass      = $config['ftp']['password'];
//$ftpfolder      = $config['ftp']['folder'];

$daemon_path = __DIR__."/..";
$babel_path  = $config['babel_path'];
$host        = $config['host'];
$pymol_path  = $config['pymol_path'];

$max_tasks   = 12;

function database_connect() {
  global $dbhost, $dbname, $dbuser, $dbpass;

  $conn = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  return $conn;
}

function database_update($id, $m1, $m2 = []) {
  global $dbtable;
  $conn = database_connect();

  $query = "update $dbtable set ";
  $tmp = [];
  foreach ($m1 as $k => $v) {
    $q = $conn->quote($v);
    $tmp[] = "$k=$q";
  }
  foreach ($m2 as $k => $v) {
    $q = $v;
    $tmp[] = "$k=$q";
  }
  $query .= implode(',', $tmp);
  $query .= " where id=$id";

  $stmt = $conn->prepare($query); 
  $stmt->execute(); 
  $conn = null;
}

function save_file($file) {
  $ext = pathinfo($file, PATHINFO_EXTENSION);
  $name = tempnam(__DIR__."/../blobs", '').".$ext";
  $cmd = "cp $file $name; chmod 777 $name";
  system($cmd);
  $name = pathinfo($name, PATHINFO_BASENAME);
  return $name;
}

//function ftp_mkdir_recursive($con_id, $path){
//  $pwd = @ftp_pwd($con_id);
//
//  $parts = explode("/",$path);
//
//  foreach($parts as $part){
//    if(empty($part)){
//      if (@ftp_chdir($con_id, "/")) {
//        continue;
//      } else {
//        @ftp_chdir($con_id, $pwd);
//        return false;
//      }
//    } else {
//      if(@ftp_chdir($con_id, $part)) {
//        continue;
//      } else {
//        if(@ftp_mkdir($con_id, $part)){
//          if (@ftp_chdir($con_id, $part)) {
//            continue;
//          } else {
//            @ftp_chdir($con_id, $pwd);
//            return false;
//          }
//        }else{
//          @ftp_chdir($con_id, $pwd);
//          $return = false;
//        }
//      }
//    }
//  }
//
//  @ftp_chdir($con_id, $pwd);
//  return true;
//}
//
//function ftp_open() {
//  global $ftphost, $ftpuser, $ftppass, $ftpfolder;
//
//  $conn = ftp_connect($ftphost) or die("Couldn't connect to $ftphost");
//  ftp_pasv($conn, true);
//  ftp_login($conn, $ftpuser, $ftppass) or die ("Couldn't login to $ftphost");
//
//  return $conn;
//}


